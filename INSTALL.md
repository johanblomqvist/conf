# Basics
```
cp ./X/.X* $(HOME)
cp ./hdw $(HOME)
sudo apt update
sudo apt install git curl
sudo apt install vim-nox cmake
sudo apt install python3-dev build-essential
```

# i3
```
sudo apt install i3
cp ./i3 ~/.config/
```

# Vim
```
cp -r ./vim/.vim $(HOME)/
```
start vim run `:PlugClean && :PlugInstall`
```
python3 $(HOME)/.vim/plugged/YouCompleteMe/install.py
```

# Tmux
```
sudo apt install tmux
cp -r ./tmux/.tmux* $(HOME)
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```
Open tmux and hit `Prefix + I (capital I)`

# Oh-my-zsh!
```
sudo apt install zsh autojump
chsh -s $(which zsh)
```
Logout and back in
```
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
cp ./zsh/.zshrc $(HOME)
```

# urxvt
```
sudo apt install rxvt-unicode
sudo update-alternatives --config x-terminal-emulator
```

# powerline fonts working with urxvt
Still a bit unclear which of these are neccessary.
```
sudo apt install fonts-powerline
mkdir ${HOME}/repos && cd ${HOME}/repos
git clone https://github.com/powerline/fonts.git
./fonts/install.sh
```

# emacs28-nox with doom
```
sudo add-apt-repository ppa:kelleyk/emacs
sudo apt update
sudo apt install emacs28-nox
sudo apt install ripgrep
sudo apt install fd-find
git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
~/.emacs.d/bin/doom install
cp -r ./doom/.doom.d ${HOME}
~/.emacs.d/bin/doom sync
```
TODO add setup for autocomplete through compiler etc

